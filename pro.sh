#!/usr/bin/env bash

_pkgs=("openssh-server vim epel-release sshpass ansible git python3 python3-pip")

#scp ~/.ssh/id_rsa.pub web@10.0.200.101:~/.ssh/authorized_keys


main(){
	sshkey
	install
#	ansiblei
	clon_git
}
sshkey(){

	ssh-keygen -t rsa -N "" -f .ssh/id_rsa
	sudo sed -i '/^PasswordAuthentication/s/no/yes/' /etc/ssh/sshd_config
	sudo systemctl restart sshd
	sudo cat id_rsa.pub >> authorized_keys 


}

install(){
	
	sudo yum update -y
	sudo yum install $_pkgs -y
	sudo pip3 install --upgrade pip\
	pip install pipenv
}

clon_git(){
   
	sudo git clone  https://gitlab.com/mishsh/ansible_basic.git
	sudo pip3 install -r /home/vagrant/ansible_basic/requirements.txt

}

user_config(){

	sudo chown -R vagrant:vagrant /home/vagrant/ansible_basic
	sudo export LC_ALL=de_DE.utf-8 ; sudo export LANG=de_DE.utf-8
}

#ansible(){

        #ansible all -u vagrant  -m ping


#}

main
